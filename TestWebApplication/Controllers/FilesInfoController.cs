﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestWebApplication.Service;
using static TestWebApplication.Service.FileService;

namespace TestWebApplication.Controllers
{
    public class FilesInfoController : ApiController
    {
        FileService _fileService;

        public FilesInfoController()
        {

        }

        public class PathQueryModel
        {
            public string Path { get; set; }
            public bool GetParent { get; set; }
        }
      
        public FilesInfoModel Get([FromUri] PathQueryModel query)
        {
            _fileService = new FileService();
            return _fileService.GetFilesInfoModel(query.Path, query.GetParent, "*");
        }

        
        
    }
}
