﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace TestWebApplication.Service
{
    public class FileService
    {

        private FilesInfoModel _fileInfoModel;

        public FileService()
        {

        }


        public List<String> GetFiles(DirectoryInfo di, string pattern)
        {

            List<string> files = new List<string>();

            try
            {
                foreach (FileInfo f in di.GetFiles(pattern))
                {
                    files.Add(f.FullName);
                }
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("Directory {0}  \n could not be accessed!!!!", di.FullName);
            }

            return files;
        }



        public List<string> GetFolders(DirectoryInfo di, string pattern)
        {


            List<string> folders = new List<string>();

            try
            {
                foreach (DirectoryInfo d in di.GetDirectories())
                {
                    folders.Add(d.FullName);
                }
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("Directory {0}  \n could not be accessed!!!!", di.FullName);
            }

            return folders;
        }


        private void fillAllFilesInfo(DirectoryInfo dir, string searchPattern)
        {

            try
            {
                foreach (FileInfo f in dir.GetFiles(searchPattern))
                {
                    var mbsize = f.Length >> 20;

                    if (mbsize <= 10)
                    {
                        _fileInfoModel.QntFilesLessThen10MB++;
                    }
                    else if (mbsize <= 50)
                    {
                        _fileInfoModel.QntFilesLessThen50MB++;
                    }
                    else if (mbsize >= 100)
                    {
                        _fileInfoModel.QntFilesMoreThen100MB++;
                    }
                }
            }

            catch
            {
                System.Diagnostics.Debug.WriteLine("Directory {0}  \n could not be accessed!!!!", dir.FullName);
                return;
            }

            foreach (DirectoryInfo d in dir.GetDirectories())
            {
                fillAllFilesInfo(d, searchPattern);
            }

        }

        public FilesInfoModel GetFilesInfoModel(string path, bool getParent, string pattern)
        {
            _fileInfoModel = new FilesInfoModel();

            DirectoryInfo dir = new DirectoryInfo(path);
            dir = getParent ? dir.Parent : dir;
            if (dir!=null&&path!="...") {                
                _fileInfoModel.Folders = this.GetFolders(dir, pattern);
                _fileInfoModel.Files = this.GetFiles(dir, pattern);
                fillAllFilesInfo(dir, pattern);
                _fileInfoModel.CurrDir = dir.FullName;
            }
            else
            {
                DriveInfo[] driverInfo = DriveInfo.GetDrives();
                _fileInfoModel.Folders = new List<string>();
                foreach (var d in driverInfo)
                {   
                    if(d.DriveType == DriveType.Fixed)
                    {
                        _fileInfoModel.Folders.Add(d.RootDirectory.FullName);
                        fillAllFilesInfo(d.RootDirectory, pattern);
                    }           
                    
                }
                _fileInfoModel.CurrDir = "...";
            }           
            

            return _fileInfoModel;

        }

        public class FilesInfoModel
        {
            public List<string> Folders { get; set; }
            public List<string> Files { get; set; }
            public int QntFilesLessThen10MB { get; set; }
            public int QntFilesLessThen50MB { get; set; }
            public int QntFilesMoreThen100MB { get; set; }
            public string CurrDir { get; set; }
        }
    }
}