﻿var app = angular.module('app', []);

app.controller('FilesInfoController', function FilesInfoController($scope, $http) {

    $scope.FilesInfo = { Files: [], Folders: [] };

    $scope.getDir = function (dir) {        
        var getParent;
        if (dir == '.../') {
            if ($scope.FilesInfo.CurrDir == "...") 
                return;   
            getParent =true;
            dir = $scope.FilesInfo.CurrDir;

        } else {
            getParent = false;
        }
        $scope.dataLoaded = false;

        $http.get('api/FilesInfo/', { params: { Path: dir, GetParent: getParent } })
            .success(function (result) {
                $scope.FilesInfo = result;
                $scope.FilesInfo.Folders.unshift(".../");
                $scope.dataLoaded = true;
            })
            .error(function (data) {
                $scope.FilesInfo = { Files: [], Folders: [] };
                console.log(data);
            })
    }

    $scope.getDir('D://');   
});